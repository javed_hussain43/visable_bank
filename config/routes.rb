# == Route Map
#
#                                Prefix Verb   URI Pattern                                                                              Controller#Action
#               balance_api_v1_accounts GET    /api/v1/accounts/balance(.:format)                                                       api/v1/accounts#balance
#              transfer_api_v1_accounts POST   /api/v1/accounts/transfer(.:format)                                                      api/v1/accounts#transfer
Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :accounts, only: [] do
      	member do
      		get 'balance'
      		post 'transfer'
      	end
      end
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
