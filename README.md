# Vision Bank App

Build a Rails application `VisableBank`. The application performs basic banking operations like money transfers and showing of current account balance.

We would like to see your way of implementing the models, API endpoints. Also your coding style is very interesting for us.

Imagine you have bank accounts with transactions and balance. You don’t need to build authentication and UI. All implementation is about the backend part.

1. You need to transfer funds between accounts. Think about what classes and DB tables you need for this task. Implement the models and a service class that wires them together.
2. Build a REST endpoint to show the balance of the account and 10 latest transactions.
3. Build a REST endpoint to transfer money between accounts. Your REST action should use the service class you built in task 1.


# The Solution

In the solution we have defined 6 tables.
- Currency => To store the currency details
- Acccount Type => To store the type of account of the customer/employee.
- Customer => To store the details of the customer
- Account => To store the information of the customer accounts
- Transaction History - It actually stores the transaction details.


1. API To fetch Account Details 

curl -X POST \
  'http://localhost:3000/api/v1/accounts/b61cea37-87ff-41f7-b453-6d709b4b19e2/transfer.json?transfered_to_account=dd4a680a-66e7-4b1d-8d39-5c8e1beedb481&amount_to_be_transfered=100000000' \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 0' \
  -H 'Host: localhost:3000' \
  -H 'cache-control: no-cache'



2. API to transfer funds

curl -X GET \
  http://localhost:3000/api/v1/accounts/cae3aecf-3293-4d1b-95ab-2241cb73ca12/balance.json \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Host: localhost:3000' \
  -H 'cache-control: no-cache'


# Steps to Run the Application
1. Update the DB details in the database.yml

2. Run rails db:create db:migrate db:seed in the terminal

3. Run rails server in the terminal


# Improvements

1. Unit test coverage

2. Store other informations like Mode of transfer, Platform(Web, ios or Android), etc

