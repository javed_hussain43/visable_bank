class CreateAccountTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :account_types, id: :uuid do |t|
      t.string :name

      t.timestamps
    end
    add_index :account_types, :name
  end
end
