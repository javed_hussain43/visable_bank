class CreateTransactionHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :transaction_histories, id: :uuid do |t|
      t.string :transaction_type
      t.date :transaction_date
      t.string :to_account_id, type: :uuid
      t.string :from_account_id, type: :uuid
      t.integer :transaction_amount
      t.references :currency, null: false, type: :uuid, foreign_key: true

      t.timestamps
    end
    add_index :transaction_histories, :transaction_type
    add_index :transaction_histories, :to_account_id
    add_index :transaction_histories, :from_account_id
  end
end
