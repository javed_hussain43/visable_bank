class CreateAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :accounts, id: :uuid do |t|
      t.references :customer, null: false, type: :uuid, foreign_key: true
      t.references :account_type, null: false, type: :uuid, foreign_key: true
      t.integer :balance_amount
      t.references :currency, null: false, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
