class CreateCurrencies < ActiveRecord::Migration[6.0]
  def change
    create_table :currencies, id: :uuid do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
    add_index :currencies, :name
  end
end
