# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
AMOUNT = 300

Customer.destroy_all
customer_1 = Customer.create(name: Faker::Name.name, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone)
customer_2 = Customer.create(name: Faker::Name.name, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone)

AccountType.destroy_all
AccountType.create([{name: 'savings'}, {name: 'current'}])

Currency.destroy_all
Currency.create([{name: 'euro'}, {name: 'dollar'}])

account_1 = customer_1.accounts.create(currency: Currency.first, account_type: AccountType.first, balance_amount: 10000)
account_2 = customer_2.accounts.create(currency: Currency.first, account_type: AccountType.first, balance_amount: 20000)

TransactionHistory.destroy_all
Accounts::Transfer.new(account_id: account_1.id, amount_to_be_transfered: AMOUNT, transfered_to_account: account_2.id).call