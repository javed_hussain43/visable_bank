# == Schema Information
#
# Table name: customers
#
#  id         :uuid             not null, primary key
#  name       :string
#  email      :string
#  phone      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Customer < ApplicationRecord
	has_many :accounts, dependent: :destroy
end
