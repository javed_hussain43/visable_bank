# == Schema Information
#
# Table name: transaction_histories
#
#  id                 :uuid             not null, primary key
#  transaction_type   :string
#  transaction_date   :date
#  to_account_id      :string
#  from_account_id    :string
#  transaction_amount :integer
#  currency_id        :uuid             not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class TransactionHistory < ApplicationRecord

	scope :recently_created, -> { order(created_at: :desc).limit(10) }

	belongs_to :account, foreign_key: :from_account_id
	
end
