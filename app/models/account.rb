# == Schema Information
#
# Table name: accounts
#
#  id              :uuid             not null, primary key
#  customer_id     :uuid             not null
#  account_type_id :uuid             not null
#  balance_amount  :integer
#  currency_id     :uuid             not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Account < ApplicationRecord
  belongs_to :customer
  belongs_to :account_type
  belongs_to :currency
  has_many :transaction_histories, class_name: 'TransactionHistory', foreign_key: 'from_account_id'
end
