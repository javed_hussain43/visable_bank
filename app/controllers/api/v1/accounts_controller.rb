class Api::V1::AccountsController < ApplicationController

	before_action :set_account, only: [:transfer, :balance]

  def transfer
  	transfered_to_account = Account.find_by(id: params[:transfered_to_account])
  	
  	unless transfered_to_account
  		result = { response_message: "Account with id=#{params[:transfered_to_account]} doesn't exist" }
  		status_code = :not_found
  	else
  		result = Accounts::Transfer.new(
			account_id: @account.id, 
			amount_to_be_transfered: params[:amount_to_be_transfered],
			transfered_to_account: transfered_to_account.id
			).call
			status_code = :ok
  	end
  	render json: result, status: status_code
  end

  def balance
  	render json: AccountSerializer.new(@account).as_json
  end

  private

  def set_account
  	@account = Account.find_by(id: params[:id])
  	unless @account
  		render json: { message: "Account with id=#{params[:id]} doesn't exist" }, status: :not_found
  	end
  end

end
