module Accounts
	class Transfer
		DEBIT_TYPE = 'debit'
		CREDIT_TYPE = 'credit'

		attr_reader :account, :amount_to_be_transfered, :transfered_to_account
		
		def initialize(account_id:, amount_to_be_transfered: , transfered_to_account:)
			@account = Account.find(account_id)
			@amount_to_be_transfered = amount_to_be_transfered.to_i
			@transfered_to_account = Account.find(transfered_to_account)
		end

		def call
			if account.balance_amount >= amount_to_be_transfered
				Account.transaction do
					begin
						account.transaction_histories.create(
							transaction_type: DEBIT_TYPE,
							transaction_date: Date.today,
							to_account_id: transfered_to_account.id,
							transaction_amount: amount_to_be_transfered,
							currency_id: account.currency_id
							)
						transfered_to_account.transaction_histories.create(
							transaction_type: CREDIT_TYPE,
							transaction_date: Date.today,
							to_account_id: account.id,
							transaction_amount: amount_to_be_transfered,
							currency_id: transfered_to_account.currency_id
							)

						account.balance_amount -= amount_to_be_transfered
						transfered_to_account.balance_amount += amount_to_be_transfered
						account.save
						transfered_to_account.save
						
					rescue Exception => e
						raise ActiveRecord::Rollback, "Call tech support!"
					end
				end
				message = 'Transfer has been successfully done.'
			else
				message = 'Insufficent Balance in your account.'
			end
			return { response_message: message }
		end

	end
end