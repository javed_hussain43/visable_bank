# == Schema Information
#
# Table name: currencies
#
#  id          :uuid             not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class CurrencySerializer < ActiveModel::Serializer
  attributes :id, :name
end
