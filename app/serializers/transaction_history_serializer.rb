# == Schema Information
#
# Table name: transaction_histories
#
#  id                 :uuid             not null, primary key
#  transaction_type   :string
#  transaction_date   :date
#  to_account_id      :string
#  from_account_id    :string
#  transaction_amount :integer
#  currency_id        :uuid             not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class TransactionHistorySerializer < ActiveModel::Serializer
  
  attributes :id, :transaction_amount, :transaction_type, :transaction_date
end
