# == Schema Information
#
# Table name: account_types
#
#  id         :uuid             not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AccountTypeSerializer < ActiveModel::Serializer
  attributes :id, :name
end
