# == Schema Information
#
# Table name: accounts
#
#  id              :uuid             not null, primary key
#  customer_id     :uuid             not null
#  account_type_id :uuid             not null
#  balance_amount  :integer
#  currency_id     :uuid             not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class AccountSerializer < ActiveModel::Serializer
	has_many :transaction_histories do
		@object.transaction_histories.recently_created
	end
  belongs_to :customer
  belongs_to :account_type
  belongs_to :currency
  attributes :id, :balance_amount
end
